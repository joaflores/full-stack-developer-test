var parkingService = require('../services/parkingService');
 
/**
 * Gets all the cars and list them all in screen.
 */
exports.listCars = function(req, res) {
    // Use the method loadCars from parkingService to get all the cars
    parkingService.loadCars(function(cars, err) {
        if (err) {
            console.error('Error al recuperar los carros');
            res.render('error', {
                message: 'Se ha producido un error.',
                error: null
            });
        } else {
            console.log('Registros recuperados:', cars);
            res.render('cars', {cars: cars});
        }
    });
};

/**
 * Displays the form to checkin a car.
 */
exports.checkin = function(req, res) {
    var car = {};
    res.render('checkin', {car: car});
};

/**
 * Creates a new car registry.
 */
exports.checkinCar = function(req, res) {
    var registration = req.body.registration;
    var cartype = req.body.cartype;
 
    parkingService.checkinCar(registration, cartype, function(car, err) {
        if (err) {
            console.error('Error al crear el registro');
            res.render('error', {
                message: 'Se ha producido un error.',
                error: null
            });
        } else {
            console.log('registro creado:', car);
            res.redirect('/cars');
        }
    });
};
