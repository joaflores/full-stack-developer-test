var http = require('http');
 
/**
 * Parámetros de la API RESTful
 */
var host = 'localhost';
var port = '3600';

exports.loadCars = function(next) {
    var path = '/api/registry';
 
    var options = {
        host: host,
        port: port,
        path: path,
        method: 'GET',
        encoding: null
    };
 
    // Se invoca el servicio RESTful con las opciones configuradas previamente y sin objeto JSON.
    callApi(options, null, function (regs, err) {
        if (err) {
            next(null, err);
        } else {
            next(regs, null);
        }
    });
};

/**
 * Función encargada de invocar la API y devolver
 * el objeto JSON correspondiente.
 */
function callApi(options, jsonObject, next) {
    var req = http.request(options, function(res) {
        var contentType = res.headers['content-type'];
 
        /**
         * Variable para guardar los datos del servicio RESTfull.
         */
        var data = '';
 
        res.on('data', function (chunk) {
            // Cada vez que se recojan datos se agregan a la variable
            data += chunk;
        }).on('end', function () {
            // Al terminar de recibir datos los procesamos
            var response = null;
 
            // Nos aseguramos de que sea tipo JSON antes de convertirlo.
            if (contentType.indexOf('application/json') != -1) {
                response = JSON.parse(data);
            }
 
            // Invocamos el next con los datos de respuesta
            next(response, null);
        })
        .on('error', function(err) {
            // Si hay errores los sacamos por consola
            console.error('Error al procesar el mensaje: ' + err)
        })
        .on('uncaughtException', function (err) {
            // Si hay alguna excepción no capturada la sacamos por consola
            console.error(err);
        });
    }).on('error', function (err) {
        // Si hay errores los sacamos por consola y le pasamos los errores a next.
        console.error('HTTP request failed: ' + err);
        next(null, err);
    });
 
    // Si la petición tiene datos estos se envían con la request
    if (jsonObject) {
        console.log('Objeto json: ' + jsonObject);
        req.write(jsonObject);
    }
    console.log(req.contentType);
    console.log(req.getHeaders);
    req.end();
};

/**
 * Función encargada de crear un nuevo registro de parqueo con los datos dados.
 */
exports.checkinCar = function(registration, cartype, next) {
    var path = '/api/registry';
 
    var carData = JSON.stringify({
        "registration": registration,
        "cartype": cartype
    });

    console.log('json: '+carData);
 
    var options = {
        host: host,
        port: port,
        path: path,
        method: 'POST',
        encoding: null
    };
 
    // Se invoca el servicio RESTful con las opciones configuradas previamente y con los datos del vehiculo.
    callApi(options, carData, function (car, err) {
        if (err) {
            next(null, err);
        } else {
            next(car, null);
        }
    });
};