var express = require('express');
var router = express.Router();
var parkingController = require('../controllers/parkingController');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Parking system' });
});

module.exports = router;

/* GET Check in page. */
router.get('/checkin', function(req, res) {
  res.render('checkin', { title: 'Check-in Car' });
});

/* GET cars listing. */
router.get('/cars', parkingController.listCars);

/* GET to show the checkin form. */
router.get('/checkin', parkingController.checkin);

/* POST car and save changes. */
router.post('/checkinCar', parkingController.checkinCar);
