const express         = require("express");
const app             = express();
const bodyParser      = require("body-parser");
const methodOverride  = require("method-override");
const mongoose        = require('mongoose');

mongoose.connect('mongodb://localhost/ParkingDB', { useNewUrlParser: true,useUnifiedTopology: true }, function(err, res) {
  if(err) throw err;
  console.log('Connected to Database');
});

var cors = require('cors');
app.use(cors());

// Middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

// Import Models and controllers
var models     = require('./model/ParkRegistry')(app, mongoose);
var parkController = require('./controller/Parking');

// root Route
var router = express.Router();
router.get('/', function(req, res) {
  res.send("API is running");
});
app.use(router);

// API routes
var parkRegs = express.Router();

parkRegs.route('/registry')
  .get(parkController.findAllRegs)
  .post(parkController.addParkRegistry);

  parkRegs.route('/registry/:registration')
  .get(parkController.findByRegistration)

app.use('/api', parkRegs);

// Start server
app.listen(3600, function() {
  console.log("API is listening on http://localhost:3600");
});