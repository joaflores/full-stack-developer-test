exports = module.exports = function(app, mongoose) {

const parkRegistrySchema = new mongoose.Schema({
    registration: { type: String },
    cartype:      { type: Number},
    hourIn:       { type: Date },
    hourOut:      { type: Date },
    totalMinutes: { type: Number },
    payment:          { type: Number}
 });

 mongoose.model('ParkRegistry', parkRegistrySchema);

};