var mongoose = require('mongoose');
var ParkRegistry  = mongoose.model('ParkRegistry');


//GET - Return all parking registries in the DB
exports.findAllRegs = function(req, res) {
    ParkRegistry.find(function(err, parkRegs) {
    if(err) res.send(500, err.message);

    console.log('GET /registry')
        res.status(200).jsonp(parkRegs);
    });
};

//GET - Return a parking registry (car) with specified registration
exports.findByRegistration = function(req, res) {
    ParkRegistry.findOne({registration : req.params.registration}, function(err, registry) {
    if(err) return res.send(500, err.message);
    
		console.log('GET /registry/' + req.params.registration);
		
		var currentDateTime = Date.now();
		var totalMinutes =	Math.round(((currentDateTime-registry.hourIn)/1000)/60);
		registry.totalMinutes=totalMinutes;
            res.status(200).jsonp(registry);
        });
};

//PUT - Checkout a car
exports.updateRegistry = function(req, res) {
	ParkRegistry.findById(req.params.id, function(err, registry) {
		registry.registration   = req.body.registration;
		registry.cartype    = req.body.cartype;
		registry.hourIn = req.body.hourIn;
		registry.hourOut  = req.body.hourOut;
		registry.totalMinutes = req.body.totalMinutes;
		registry.payment   = registry.totalMinutes * 0.05;
	

		registry.save(function(err) {
			if(err) return res.send(500, err.message);
      res.status(200).jsonp(registry);
		});
	});
};


//POST - Insert a new registry in the DB
exports.addParkRegistry = function(req, res) {
	console.log('POST');
	console.log(req.body);

	var dateIn = Date.now();

	var parkReg = new ParkRegistry({
		registration: req.body.registration,
		cartype: 	  req.body.cartype,
		hourIn:       dateIn,
		hourOut:      '',
		totalMinutes: 0,
		payment:	  0
	});

	parkReg.save(function(err, parkReg) {
		if(err) return res.send(500, err.message);
    res.status(200).jsonp(parkReg);
	});
};